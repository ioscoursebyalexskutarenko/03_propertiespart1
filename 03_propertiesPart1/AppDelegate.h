//
//  AppDelegate.h
//  03_propertiesPart1
//
//  Created by Admin on 24.12.16.
//  Copyright © 2016 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

